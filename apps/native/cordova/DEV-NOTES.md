# Cordova development notes

List of useful stuffs I've found...

# Project init

## How to add android to the project ?
```bash
cordova platform add android@6.4.0
```

# Emulators

## What is the list of emulators, and how to launch them ?

```bash
cordova run --list --emulator
cordova run ios || cordova run android
cordova emulate ios --target="iPhone-SE, 11.2"
```

## Why don't I get the Safari Remote Web Debugger when I launch the Simulator as stated above ?

Apparently, it must be launched via XCode. But maybe not (just try restart Safari instead ?)

## I have the following issue when I launch the iOS simulator...
```
No target specified for emulator. Deploying to undefined simulator
Device type "com.apple.CoreSimulator.SimDeviceType.undefined" could not be found.
```

Solution is you have to type:
```bash
cd platforms/ios/cordova && npm install ios-sim@latest
```

## What are the existing android versions ?
C.f. https://developer.android.com/guide/topics/manifest/uses-sdk-element.html#ApiLevels

## Also, whar are the relative number of devices that share a certain characteristic, such as Android version ?
C.f. https://developer.android.com/about/dashboards/index.html#Platform

## Typing `cordova run android` leads to `Error: Cannot read property 'replace' of undefined`
There is a Cordova bug:
https://forum.ionicframework.com/t/error-cannot-read-property-replace-of-undefined-android/93297/27

The target is Android API 26 and it is trying to parse it with this:
```javascript
var num = target.split('(API level ')[1].replace(')', '');
Which of course fails cause it should.
```

So type:
```bash
cordova platform rm Android
cordova platform add Android@6.0.0
```
# Android Studio and HAXM

## How to start or stop HAXM ?
To stop or start Intel HAXM, use the following commands:
- Stop:  `sudo kextunload –b com.intel.kext.intelhaxm`
- Start: `sudo kextload –b com.intel.kext.intelhaxm`
