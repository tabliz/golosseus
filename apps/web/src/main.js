/*
 * Temporary note: package.json was initially:
 *   "framework7": "^2.3.1",  // Was ^2.0.7
 *   "framework7-vue": "^2.3.0",  // Was ^2.0.7
 */

/*
 * Little docs of the query string parameters supported in URL:
 *  theme=ios|md|auto : F7 theme to apply
 *  delayResponse=75  : delay to add onto API mocked queries
 *  debug=true        : Will display the debug panel UI, nad enable the debug logs
 * Eg:
 *   http://localhost:8080/?theme=ios&delayResponse=75&debug=true
 */

/*
 * Interesting readings:
 * https://applikeysolutions.com/blog/the-guide-to-restaurant-app-development
 */

// Imports utils
import Device from 'current-device';

// Import Vue and components
import Vue from 'vue';
import VueAnalytics from 'vue-ua';

import Framework7 from 'framework7/dist/framework7.esm.bundle';
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle';
import { StarRating } from 'vue-rate-it';

// HTTP lib
import Axios from 'axios';
import 'axios-response-logger';

// i18n
import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import VueI18Next from '@panter/vue-i18next';

// Import Framework7 styles
import 'framework7/dist/css/framework7.css';

// Import Tabliz styles
import './css/icons.css';
import './css/app.css';

// Import Tabliz code
import myApiMocks from './js/api-mocks';
import HTTP from './js/http-constants';
import myResources from './js/langs';
import myRoutes from './routes';
import { GETTERS, MUTATIONS } from './js/store-constants';
import myStore from './js/store';
import { DebugQS, FastQueryStringParsing } from './js/utils-common';

// Import App Component
import App from './app.vue';
import DebugBlock from './components/DebugBlock.vue';

// Let's start !
const D = DebugQS('main.js');
D('Tabliz ignition!');

// Parse query string parameters
const PARAM_THEME = FastQueryStringParsing('theme');
const PARAM_DEBUG = Boolean(FastQueryStringParsing('debug', 'false').toLowerCase() === 'true');
const PARAM_DELAY_RESPONSE = Number(FastQueryStringParsing('delayResponse', '0'));

// What theme are we gonna use?
// (Framework7 does not automatically select ios theme on macOS)
let myTheme = (Device.desktop() && Device.macos()) ? 'ios' : 'auto'; // 'auto', 'ios' or 'md'
D(`Detected theme: '${myTheme}'`);

// Override the theme to use according to browser's URL Query String
// E.g. for testing themes, append '?theme=ios' or '?theme=md' to force theme
if (PARAM_THEME.length > 0 && myTheme !== PARAM_THEME) {
  // Weird bug above when using !==, != and !()
  D(`Theme overriden from query string: '${PARAM_THEME}'`);
  myTheme = PARAM_THEME;
}

// Also look for the debug switch in URL (used after Vue app init)
D(`Debug mode overridden from query string: ${PARAM_DEBUG}`);

// Init Framework7
Vue.use(Framework7Vue, Framework7);

// Init analytics
Vue.use(VueAnalytics, {
  appName: 'Tabliz',
  appVersion: '1.0.0',
  trackingId: 'UA-113565299-2',
  debug: PARAM_DEBUG,
  trackPage: false,
});

// Register Vue components: star-rating, debug-block and vue-friendly-iframe
Vue.component('star-rating', StarRating);
Vue.component('debug-block', DebugBlock);

// Init i18n
Vue.use(VueI18Next);
i18next
  .use(LanguageDetector)
  .init({
    debug: PARAM_DEBUG,
    // lng: 'fr',
    // fallbackLng: 'en',
    resources: myResources,
    parseMissingKeyHandler: function _parseMissingKeyHandler(key) {
      // We return '?? lng: key ??' when the key is not found
      const missingValue = `?? ${i18next.language}:${key} ??`;
      D(`parseMissingKeyHandler called with ${key}, returning ${missingValue}`);
      return missingValue;
    },
  });
const i18n = new VueI18Next(i18next);

// Make available axios in $http
Vue.prototype.$http = Axios;
Vue.prototype.$getHttpConfig = function _getHttpConfig() {
  const currentLng = myStore.getters[GETTERS.CURRENT_LNG];
  const config = {
    headers: {
      [HTTP.ACCEPT_LANGUAGE]: currentLng,
    },
  };
  D('Http config returned for currentLng "%s" is: %o', currentLng, config);
  return config;
};

// Set API mocks
myApiMocks.setMockers(Axios, {
  delayResponse: PARAM_DELAY_RESPONSE,
});
Vue.config.devtools = true;

// Init App
const ROOT_VM = new Vue({ // eslint-disable-line no-unused-vars
  el: '#app',
  i18n,
  store: myStore,
  // Register App Component
  components: {
    app: App,
  },
  template: '<app/>',
  // Init Framework7 by passing parameters here
  framework7: {
    actions: {
      closeByBackdropClick: false,
      closeByOutsideClick: false,
      convertToPopover: true,
    },
    id: 'com.tabliz.apps.vueTabliz', // App bundle ID
    name: 'Tabliz', // App name
    on: {
      init: function _init() {
        D('F7.init() called here.');
      },
      pageInit: function _pageInit() {
        D('F7.pageInit() called here.');
      },
    },
    routes: myRoutes,
    theme: myTheme,
    version: '1.0.0',
  },
});

// Store the initial launch time
myStore.commit(MUTATIONS.ANALYTICS.SET_SESSION_START_TIME, new Date());

// Store the debug state
myStore.commit(MUTATIONS.SET_DEBUG, PARAM_DEBUG);

// Store the detected language
myStore.commit(MUTATIONS.SET_CURRENT_LNG, i18next.language.substring(0, 2));
