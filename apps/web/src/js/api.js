import { MUTATIONS } from './store-constants';
import { DebugQS } from './utils-common';
import UtilsUi from './utils-ui';

const D = DebugQS('api.js');

export const PARAMS = Object.freeze({
  RID: ':rId', // Restaurant Id
  MID: ':mIId', // Menu Item Id
});

export const PATHS = Object.freeze({
  MENU_ITEM: `/r/${PARAMS.RID}/mi/${PARAMS.MIID}`,
  RESTAURANTS: '/restaurants/',
  RESTAURANT: `/r/${PARAMS.RID}`,
});

const getRestaurantMenu = (vm, rId, activityText) => {
  // Builds URI with parameters
  const uri = PATHS.RESTAURANT.replace(PARAMS.RID, rId);
  D('before getRestaurantMenu with uri "%s"', uri);

  // Performs API request
  UtilsUi.showLoaderWidget(vm, activityText);
  vm.$http.get(uri, vm.$getHttpConfig())
    .then((response) => {
      D('getRestaurantMenu JSON API response is %s length', JSON.stringify(response.data).length);
      vm.$store.commit(MUTATIONS.SET_RESTAURANT_MENU, response.data);
      UtilsUi.hideLoaderWidget(vm);
    })
    .catch((error) => { // eslint-disable-line no-unused-vars
      D('getRestaurantMenu raised an error for "%s". Did you add the mock object for it? Error message was: "%s"', uri, error);
      UtilsUi.hideLoaderWidget(vm);
      vm.$f7.popup.open('#popup-error');
    });
  D('getRestaurantMenu after uri "%s"', uri);
};

const getRestaurants = (vm, activityText) => {
  D(PATHS.RESTAURANTS);

  // Performs API request
  UtilsUi.showLoaderWidget(vm, activityText);
  vm.$http.get(PATHS.RESTAURANTS, vm.$getHttpConfig())
    .then((response) => {
      vm.$store.commit(MUTATIONS.SET_RESTAURANTS, response.data.restaurants);
      UtilsUi.hideLoaderWidget(vm);
    })
    .catch((error) => { // eslint-disable-line no-unused-vars
      UtilsUi.hideLoaderWidget(vm);
      vm.$f7.popup.open('#popup-error');
    });
};

export default {
  getRestaurantMenu,
  getRestaurants,
};
