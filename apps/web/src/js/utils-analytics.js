import { DebugQS } from './utils-common';

const D = DebugQS('utils-analytics.js');

export const ScreenNames = {
  HOME: 'Home',
  RESTAURANT: 'Restaurant',
  MENU_ITEM: 'Menu Item',
  SETTINGS: 'Settings',
  SETTINGS_LNG: 'Settings Lng',
  ANSWER_SURVEY: 'Answer Survey',
  ASK_QUESTION: 'Ask Question',
  SUBSCRIBE: 'Subscribe',
  NOT_FOUND: 'Not Found',
};

export const Events = {
  Category: {
    NAVIGATION: 'Navigation',
  },
  Action: {
    BACK_RESTAURANT_2_HOME: 'backRestaurant2Home',
    BACK_MENUITEM_2_RESTAURANT: 'backMenuItem2Restaurant',
    BACK_SETTINGS_2_HOME: 'backSettings2Home',
    BACK_SETTINGS_LNG_2_HOME: 'backSettings2Home',

    SELECT_RESTAURANT: 'selectRestaurant',
    SELECT_MENU_ITEM: 'selectMenuItem',
    SELECT_SETTINGS: 'selectSettings',
    SELECT_SETTINGS_LNG: 'selectSettingsLng',
    SELECT_SETTINGS_LNG_CHOICE: 'selectSettingsLanguageChoice',
    SELECT_SETTINGS_LNG_CHOICE_CONFIRM: 'selectSettingsLanguageConfirm',
    SELECT_SETTINGS_LNG_CHOICE_CANCEL: 'selectSettingsLanguageCancel',

    SURVEY_PROMPT_USER: 'promptUser',
    SURVEY_ACCEPT: 'surveyAccept,',
    SURVEY_DECLINE: 'surveyDecline,',

    SWYPE_MENU_CAT_NEXT: 'swypeMenuCategoryNext',
    SWYPE_MENU_CAT_PREV: 'swypeMenuCategoryPrev',
  },
};

export const Tracker = {
  trackEvent: function _trackEvent(vm, cat, ev, label, value) {
    // Track the given event
    vm.$ua.trackEvent(cat, ev, label, value);
    D('Tracked the event with category "%s", event "%s", label "%s", value "%d"', cat, ev, label, value);
  },
  trackView: function _trackView(vm, screenName) {
    // Track the given view
    vm.$ua.trackView(screenName);
    D('Sent a track view with screenName "%s"', screenName);
  },
};

