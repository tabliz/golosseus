// import Debug from 'debug';
import MockAdapter from 'axios-mock-adapter';

import { PARAMS, PATHS } from './api';
import HTTP from './http-constants';
import { DebugQS } from './utils-common';

import M_RESTAURANTS from './mock/restaurants/_';
import M_R1 from './mock/r/1/_';
import M_R2 from './mock/r/2/_';
import M_R4 from './mock/r/4/_';
import M_R100 from './mock/r/100/_';
import M_R101 from './mock/r/101/_';
import M_R102 from './mock/r/102/_';
import M_R103 from './mock/r/103/_';
import M_R104 from './mock/r/104/_';
import M_R105 from './mock/r/105/_';
import M_R106 from './mock/r/106/_';
import M_R107 from './mock/r/107/_';
import M_R108 from './mock/r/108/_';
import M_R109 from './mock/r/109/_';
import M_R110 from './mock/r/110/_';
import M_R111 from './mock/r/111/_';
import M_R112 from './mock/r/112/_';
import M_R113 from './mock/r/113/_';
import M_R114 from './mock/r/114/_';
import M_R115 from './mock/r/115/_';
import M_R116 from './mock/r/116/_';
import M_R117 from './mock/r/117/_';
import M_R118 from './mock/r/118/_';
import M_R119 from './mock/r/119/_';
import M_R120 from './mock/r/120/_';
import M_R121 from './mock/r/121/_';
import M_R122 from './mock/r/122/_';
import M_R123 from './mock/r/123/_';
import M_R124 from './mock/r/124/_';
import M_R125 from './mock/r/125/_';
import M_R126 from './mock/r/126/_';
import M_R127 from './mock/r/127/_';

const D = DebugQS('api-mocks.js');
const DEFAULT_LANGUAGE = 'fr';

function getAcceptLanguage(config) {
  let al = config.headers[HTTP.ACCEPT_LANGUAGE];
  if (al === undefined || al == null || al.length === 0) {
    al = DEFAULT_LANGUAGE;
  }
  D('Accept Language is %s', al);
  return al;
}

function getHttpResponseHeaders(al) {
  return {
    headers: {
      [HTTP.CONTENT_LANGUAGE]: al,
    },
  };
}

function getLocalizedResponse(responseByLang, config) {
  const AL = (responseByLang[getAcceptLanguage(config)] !== undefined)
    ? getAcceptLanguage(config) // Use the provided language
    : DEFAULT_LANGUAGE; // Otherwise use the default one
  return [200, responseByLang[AL], getHttpResponseHeaders(AL)];
}

const setMockers = (Axios, mockerConfig) => {
  const mock = new MockAdapter(Axios, mockerConfig);

  // List of restaurants
  mock.onGet(PATHS.RESTAURANTS).reply((config) => {
    const HTTP_RESPONSE = getLocalizedResponse(M_RESTAURANTS, config);
    D('getHttpResponseRestaurants returns %o', HTTP_RESPONSE);
    return HTTP_RESPONSE;
  });

  // All restaurants details
  [
    [1, M_R1], // Le Phare
    [2, M_R2], // Le Trèfle
    [3, M_R1], // East Street (same than Le Phare)
    [4, M_R4], // Romagna Mia

    [100, M_R100], // PortoVenere
    [101, M_R101], // Bazar Cafe
    [102, M_R102], // Byblo's
    [103, M_R103], // Le Café de Nice
    [104, M_R104], // Club Player Caffé
    [105, M_R105], // La Lorraine
    [106, M_R106], // Coté Lounge
    [107, M_R107], // Gilardi
    [108, M_R108], // Il Vicoletto
    [109, M_R109], // Jach
    [110, M_R110], // La Cucina
    [111, M_R111], // La Femme Du Boulanger
    [112, M_R112], // La Massaia
    [113, M_R113], // La Pizza Cresci
    [114, M_R114], // La Taverne Massena
    [115, M_R115], // Le Lodge
    [116, M_R116], // Le Magenta
    [117, M_R117], // Le Québec
    [118, M_R118], // Les2 Terrasses
    [119, M_R119], // Le Saint Valentin
    [120, M_R120], // Lobsta
    [121, M_R121], // Mama
    [122, M_R122], // Milos
    [123, M_R123], // Moris Bar
    [124, M_R124], // Pinnochio
    [125, M_R125], // Rina
    [126, M_R126], // Sapori
    [127, M_R127], // VIPs Grill
  ].forEach((r) => {
    mock.onGet(PATHS.RESTAURANT.replace(PARAMS.RID, r[0])).reply((config) => {
      const HTTP_RESPONSE = getLocalizedResponse(r[1], config);
      D('Returns mock restaurant %o', HTTP_RESPONSE);
      return HTTP_RESPONSE;
    });
  });

  D('Applied the mockers');
  return mock;
};

export default {
  setMockers,
};
