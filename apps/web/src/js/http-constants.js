export default {
  // Request headers
  ACCEPT_LANGUAGE: 'Accept-Language',

  // Response headers
  CONTENT_LANGUAGE: 'Content-Language',
};

