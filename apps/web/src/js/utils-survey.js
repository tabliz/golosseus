import TimeMe from 'timeme.js';
import { DebugQS } from './utils-common';

const D = DebugQS('utils-survey.js');

const CONSTANTS = {
  DEFAULT_SURVEY: {
    id: 0,
    taken: false,
    declinedCount: 0,
  },
  LATEST_SURVEY: 'latest-survey',
  SECONDS_BEFORE_SURVEY: 20,
  SECONDS_BEFORE_SURVEY_RANDOM_DELTA: 1,
  MAX_DECLINE: 3,
};

const saveSurveyState = function _saveSurveyState(surveyState) {
  localStorage.setItem(CONSTANTS.LATEST_SURVEY, JSON.stringify(surveyState));
  D('Saved survey state: %j', surveyState);
};

const getLatestSurveyState = function _getLatestSurveyState() {
  // Did we save some surveys state before ?
  let surveyState = null;
  if (localStorage.getItem(CONSTANTS.LATEST_SURVEY)) {
    try {
      surveyState = JSON.parse(localStorage.getItem(CONSTANTS.LATEST_SURVEY));
      D('Restored latest survey state: %j', surveyState);
    } catch (e) {
      localStorage.removeItem(CONSTANTS.LATEST_SURVEY); // In case of errors, just get rid of it
      surveyState = CONSTANTS.DEFAULT_SURVEY;
      D('Caught an exception when retrieving the last survey from local storage. Recreating a default survey state: %o', surveyState);
      saveSurveyState(surveyState);
    }
  } else {
    // This is the default latestSurvey state
    surveyState = CONSTANTS.DEFAULT_SURVEY;
    D('Created default survey state: %o', surveyState);
    saveSurveyState(surveyState);
  }
  return surveyState;
};

const scheduleSurvey = function _scheduleSurvey(vm) {
  // Init only first time
  if (TimeMe.timeElapsedCallbacks.length === 0) {
    TimeMe.initialize({
      currentPageName: 'Tabliz', // current page
      idleTimeoutInSeconds: 5,
    });
  }

  // The little helper that will prompt the user
  function askUser() {
    vm.$f7.actions.open('.actions-survey', true);
  }

  // How long to wait for before prompting the user?
  const nextDuration = CONSTANTS.SECONDS_BEFORE_SURVEY
    + Math.round(Math.random() * CONSTANTS.SECONDS_BEFORE_SURVEY_RANDOM_DELTA);

  // Next absolute elapsed time
  const nextElapsedSeconds = TimeMe.getTimeOnCurrentPageInSeconds() + nextDuration;
  TimeMe.callAfterTimeElapsedInSeconds(nextElapsedSeconds, askUser);
  D('Scheduled next survey prompt in %d seconds (is %d, starting from beginning of session)', nextDuration, nextElapsedSeconds);
};

const init = function _init(vm) {
  const latestSurveyState = getLatestSurveyState();
  if (latestSurveyState.taken) {
    D('Latest survey with id %d has already been taken. No need to reschedule it.', latestSurveyState.id);
  } else if (latestSurveyState.declinedCount < 3) {
    D('Latest survey with id %d was not taken, and declined %d times: schedule it!', latestSurveyState.id, latestSurveyState.declinedCount);
    scheduleSurvey(vm);
  } else {
    D('Latest survey with id %d was not taken, but declined already 3 times: abandon....');
  }
};

export default {
  CONSTANTS,
  init,
  getLatestSurveyState,
  saveSurveyState,
  scheduleSurvey,
};
