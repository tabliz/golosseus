/* eslint max-len: ["error", { "comments": 110 }] */
/* eslint max-len: ["error", { "code": 110 }] */
/* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["state"] }] */

import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

import { /* ACTIONS, */ MUTATIONS, GETTERS } from './store-constants';
import { DebugQS, FastQueryStringBooleanParsing } from './utils-common';
import UtilsRestaurant from './utils-restaurant';

const D = DebugQS('store.js');

// Common accessors

const getRestaurantFromChosenRestaurantId = (state) => {
  D('Helper - Looking for restaurant whose id is %s', state.chosenRestaurantId);
  let restaurant = null;
  if (state.chosenRestaurantId == null) {
    D('Helper - Cancelled because of "null" id');
  } else {
    restaurant = state.restaurants.find(r =>
      (r.id === state.chosenRestaurantId));
    D('Helper - Returns restaurant "%s"', restaurant.name);
  }
  return restaurant;
};

// Getters are functions, as computed properties, but cached based on their deps.
// They get the state as 1st argument, and are exposed as store.getters.<name>
const getters = {
  [GETTERS.ANALYTICS.SESSION_START_TIME]: (state) => {
    D(`Getter - Analytics start time: '${state.analytics.startTime}'`);
    return state.analytics.startTime;
  },
  [GETTERS.CHOSEN_LNG_TO_CONFIRM]: (state) => {
    D(`Getter - chosen lng to confirm: '${state.chosenLngToConfirm}'`);
    return state.chosenLngToConfirm;
  },
  [GETTERS.CURRENT_LNG]: (state) => {
    D(`Getter - currentLng: '${state.currentLng}'`);
    return state.currentLng;
  },
  [GETTERS.DATA_OK_TO_LOAD]: (state) => {
    D(`Getter - dataOkToLoad: '${state.dataOkToLoad}'`);
    return state.dataOkToLoad;
  },
  [GETTERS.DEBUG]: (state) => {
    D(`Getter - debug: '${state.debug}'`);
    return state.debug;
  },
  [GETTERS.IS_LOADING]: (state) => {
    D(`Getter - isLoading: '${state.isLoading}'`);
    return state.isLoading;
  },
  [GETTERS.MENU_ITEM]: (state) => {
    let menuItem = null;
    if (state.chosenMenuItemFullId == null) {
      D('Getter - chosenMenuItemFullId "null" cancels operation');
    } else {
      const restaurant = getRestaurantFromChosenRestaurantId(state);
      if (!restaurant) {
        throw new Error(
          'Getter - Cannot find menu item fullID %s in restaurant "%s"',
          state.chosenMenuItemFullId,
          restaurant,
        );
      } else if (!restaurant.menu) {
        D(
          'Getter - Restaurant "%s" has menu "%s", cancels operation',
          restaurant.name,
          restaurant.menu,
        );
      } else if (!restaurant.menu.pages) {
        D(
          'Getter - Restaurant "%s" has pages "%s" in its menu, cancels operation',
          restaurant.name,
          restaurant.menu.pages,
        );
      } else {
        D(
          'Getter - Looking for menu item fullId %s, in %d pages of menu',
          state.chosenMenuItemFullId,
          restaurant.menu.pages.length,
        );
        /*      restaurant.menu.pages.find((page) => {
          menuItem = page.items.find(item =>
            (state.chosenMenuItemFullId === item.id));
          return menuItem !== undefined;
        });
        */
        const pID = UtilsRestaurant.getMenuPageIdFromFullID(state.chosenMenuItemFullId);
        D('Got pageId %s from fullId %s', pID, state.chosenMenuItemFullId);
        const chosenPage = restaurant.menu.pages.find(page => page.id === pID);
        const miID = UtilsRestaurant.getMenuItemIdFromFullID(state.chosenMenuItemFullId);
        D('Got miId %s from fullId %s', miID, state.chosenMenuItemFullId);
        menuItem = chosenPage.items.find(item => item.id === miID);
        D('Got menu item %j from fullID %s', menuItem, state.chosenMenuItemFullId);
      }
    }
    D('Getter - menuItem is %j', menuItem);
    return menuItem;
  },
  [GETTERS.RESTAURANT]: (state) => {
    const restaurant = getRestaurantFromChosenRestaurantId(state);
    return restaurant;
  },
  [GETTERS.RESTAURANT_MENU]: (state) => {
    const restaurant = getRestaurantFromChosenRestaurantId(state);
    let menu = {};
    if (!restaurant) {
      D('Getter - Chosen restaurant is %s.', restaurant);
    } else if (!restaurant.menu) {
      D('Getter - No menu in restaurant %s, restaurant.menu is %s.', restaurant.name, restaurant.menu);
    } else {
      ({ menu } = restaurant);
      // Is menu empty ?
      if (Object.keys(menu).length === 0 && menu.constructor === Object) {
        D('Getter - Menu for restaurant %s is %j', restaurant.name, restaurant.menu);
        menu.pages = [];
        D('Getter - Corrected menu is now %j', restaurant.name, restaurant.menu);
      } else {
        // D('Getter - menu %j', menu);
        D('Getter - %d pages of menu', menu.pages.length);
      }
    }
    return menu;
  },
  [GETTERS.RESTAURANT_ID]: (state) => {
    const { chosenRestaurantId: id } = state;
    return id;
  },
  [GETTERS.RESTAURANTS_COUNT]: (state) => {
    const { length: l } = state.restaurants;
    return l;
  },
};

  /*
    Mutations are operations that actually mutates the state. Each mutation handler gets the entire state tree
    as the first argument, followed by additional payload arguments.
    Mutations must be synchronous and can be recorded by plugins for debugging purposes.
    They are usable via store.commit('name')
    MUST BE synchronous.

    */
const mutations = {
  [MUTATIONS.ANALYTICS.SET_SESSION_START_TIME]: (state, startTime) => {
    state.analytics.startTime = startTime;
  },
  [MUTATIONS.SET_CHOSEN_MENU_ITEM_FULL_ID]: (state, mI) => {
    state.chosenMenuItemFullId = mI;
  },
  [MUTATIONS.SET_CURRENT_LNG]: (state, lng) => {
    state.currentLng = lng;
  },
  [MUTATIONS.SET_CHOSEN_LNG_TO_CONFIRM]: (state, lng) => {
    state.chosenLngToConfirm = lng;
  },
  [MUTATIONS.SET_CHOSEN_RESTAURANT_ID]: (state, rId) => {
    state.chosenRestaurantId = rId;
  },
  [MUTATIONS.SET_DATA_OK_TO_LOAD]: (state, okToLoad) => {
    state.dataOkToLoad = okToLoad;
  },
  [MUTATIONS.SET_DEBUG]: (state, debug) => {
    state.debug = debug;
  },
  [MUTATIONS.SET_IS_LOADING]: (state, isLoading) => {
    state.isLoading = isLoading;
  },
  [MUTATIONS.SET_RESTAURANT_MENU]: (state, apiResponse) => {
    const restaurant = getRestaurantFromChosenRestaurantId(state);
    if (!restaurant) {
      throw new Error(`Restaurant with id ${state.chosenRestaurantId} is unknown`); // eslint-disable-line max-len
    } else {
      D(
        'Mutation - Before setting menu, restaurant "%s" has a menu %j',
        restaurant.name,
        restaurant.menu,
      );
      const response = apiResponse;
      if (!response.pages) {
        D('Mutation - apiResponse has no pages element, using empty array instead');
        response.pages = [];
      }
      D('Mutation - Setting for restaurant "%s" %d pages of menu', restaurant.name, response.pages.length);
      // This is how to do extend an object of the store with a new property:
      Vue.set(restaurant, 'menu', response);
      /*
         * Alternatively, one could write apparently
         * menu = { ...restaurant, menu: menu }
         * (c.f.https://vuex.vuejs.org/guide/mutations.html)
         */
      D(
        'Mutation - After setting menu, rest. "%d" has a menu of length %d',
        restaurant.name,
        JSON.stringify(restaurant.menu).length,
      );
    }
  },
  [MUTATIONS.SET_RESTAURANTS]: (state, restaurants) => {
    state.restaurants = restaurants;
    state.dataOkToLoad = false;
  },
};

  // Actions are functions that cause side effects and can involve asynchronous operations.
  // They commit the state (not mutate).
  // They received context as 1st argument: they do context.commit('name'), context.state or context.getters
const actions = {
  /*
      [ACTIONS.LOAD_RESTAURANTS](context) {
        return new Promise((resolve) => {
          this.$http.get('...')
            .then((response) => {
              context.commit(MUTATIONS.SET_RESTAURANTS, response.data);
              resolve();
            })
            .catch((error) => {
              if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                d.log(error.response.data);
                d.log(error.response.status);
                d.log(error.response.headers);
              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                d.log(error.request);
              } else {
                // Something happened in setting up the request that triggered an Error
                D('Store action - Error', error.message);
              }
              d.log(error.config);
            });
        });
      },
    */
};

  // A basic logger
const logger = createLogger({
  collapsed: true, // auto-expand logged mutations

  // eslint-disable-next-line no-unused-vars
  filter(mutation, stateBefore, stateAfter) {
    return true;
  },
  transformer(state) {
    // transforme l'état avant de le logguer.
    // retourne par exemple seulement un sous-arbre spécifique
    return state;
  },
  mutationTransformer(mutation) {
    // les mutations sont logguées au format `{ type, payload }`
    // nous pouvons les formater comme nous le souhaitons.
    return mutation; // mutation..type
  },
  logger: console, // implementation de l'API `console`, par défaut `console`
});

// Decide to switch on the logger or not
const pluginsList = [];
if (FastQueryStringBooleanParsing('debug')) {
  pluginsList.push(logger);
}

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
Vue.use(Vuex);

const strictMode = process.env.NODE_ENV !== 'production';
D('strict mode is: %s', strictMode);

export default new Vuex.Store({
  // Root state object. Each Vuex instance is just a single state tree.
  state: {
    analytics: {
      startTime: 0,
    },
    dataOkToLoad: true,
    debug: false,
    chosenRestaurantId: null,
    chosenMenuItemFullId: null,
    isLoading: false,
    chosenLngToConfirm: '',
    currentLng: '',
    restaurants: [],
  },
  getters,
  actions,
  mutations,
  plugins: pluginsList,
  strict: strictMode,
});
