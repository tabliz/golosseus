#/bin/bash

# $1 is the existing directory to inspect
# $2 is the function to call on the iterated files.
# The latter must be a function that accept:
#   the base filename iterated as $1
#   the restaurant ID as $2
#   the restaurant name as $3
function iterate() {
  for file in "${1}/"*.xlsm.xlsx
  do
    # Normalize name and parse fields
    BNAME=$(basename "$file")

    # Parse fields
    ID=NOTFOUND
    RNAME=NOTFOUND
    if [[ "$BNAME" =~ ([0-9]+)\ -\ (.*)\.xlsm\.xlsx ]]
    then
      ID=${BASH_REMATCH[1]}
      RNAME=${BASH_REMATCH[2]}
    fi

    #  Call the iterator with the expected params
    "$2" "$BNAME" "$ID" "$RNAME"
  done
}

# $1 is the base filename
# $2 is the restaurant ID
# $3 is the restaurant name
function iter_list() {
  echo "Base name is: $1"
  echo "ID is       : $2"
  echo "NAME is     : $3"
  echo
}

function iter_createMocks() {
  # Add the new mock in restaurants/
  template="./restaurants/0.json.template"
  restaurant="./restaurants/${2} - ${3}.json"
  if [[ -f "$restaurant" ]]
  then
    echo >&2 "Cannot create restaurant summary $restaurant. It already exists. Skipped."
  else
    cp "$template" "$restaurant"

    # Replace ID
    sed -i '' "s/@@ID@@/${2}/g" "$restaurant"

    # Replace name
    unCamelName=$(echo "$3" |  sed 's/\([A-Z]\)/ \1/g')
    sed -i '' "s/@@NAME@@/${unCamelName}/g" "$restaurant"
    sed -i '' "s/name\": \" /name\": \"/g"  "$restaurant" # Remove the starting space in name
    echo "Created restaurant summary mock: $restaurant"
  fi

  # Add the new mock in r/
  r="./r/${2}"
  cp -R ./r/0.template/ "$r"
  echo "Created restaurant menu mock: $r"
}

function iter_createExcelerator() {
  SRC="../../../../../tools/excelerator/excelerator-template.xlsm"
  DEST="../../../../../menus/$2 - $3.xlsm"
  if [[ -f "$DEST" ]]
  then
    echo >&2 "Cannot create the excel file $DEST. It already exists. Skipped."
  else
    echo cp "$SRC" "$DEST"
    cp "$SRC" "$DEST"
  fi
}

function iter_genRestaurantsMocksImports() {
  echo "import $3$2 from './$2 - $3.json';"
}

function iter_genRestaurantsMocksConstants() {
  echo "    $3$2,"
}

function iter_genAPIMocksImports() {
  echo "import M_R${2} from './mock/r/${2}/_';"
}

function iter_genAPIMocksConstants() {
  unCamelName=$(echo "$3" |  sed 's/\([A-Z]\)/ \1/g')
  echo "        [${2}, M_R${2}], //${name}"
}

function iter_listCodeCommands() {
  restaurant="./restaurants/${2} - ${3}.json"
  r="./r/${2}"
  echo "code --reuse-window $restaurant ${r}/*.json"
}

function main() {
  # We expect one argument only
  if [[ $# -ne 1 ]]
  then
    echo >&2 "Error: missing the existing directory as argument"
    exit 1
  fi

  # Given folder must exist
  if [[ ! -d "$1" ]]
  then
    echo >&2 "Error: directory \"$1\" does not exists (or is not a directory)"
    exit 2
  fi

  # Check we're run from our own folder
  declare -r SCRIPTDIR="$(cd `dirname ${SCRIPTNAME}` && pwd)"
  if [[ "$PWD" != "$SCRIPTDIR" ]]
  then
    echo >&2 "Error: script must be invoked from its directory"
  fi

  # List the files
  echo "List the files"
  echo "--------------"
  iterate "$1" iter_list
  echo

  # Add the mocks
  echo "Add the mocks files"
  echo "-------------------"
  iterate "$1" iter_createMocks
  echo

  # Add the Excelerator files
  echo "Add the excelerator files"
  echo "------------------------"
  iterate "$1" iter_createExcelerator
  echo

  # Print the list of imports for restaurants/_.js
  echo "restaurants/_.js: list of imports to add"
  echo "----------------------------------------"
  iterate "$1" iter_genRestaurantsMocksImports
  echo

  # Print the list of constants for restaurants/_.js
  echo "restaurants/_.js: list of constants to add"
  echo "------------------------------------------"
  iterate "$1" iter_genRestaurantsMocksConstants
  echo

  # Print the list of imports for api-mocks.js
  echo "api-mocks.js: list of imports to add"
  echo "------------------------------------"
  iterate "$1" iter_genAPIMocksImports
  echo

  # Print the list of constants for api-mocks.js
  echo "api-mocks.js: list of constants to add"
  echo "--------------------------------------"
  iterate "$1" iter_genAPIMocksConstants
  echo

  # Show the command to laucnh to edit created files
  echo "Type the following to open the corresponding files"
  echo "--------------------------------------------------"
  echo "code --reuse-window ./restaurants/_.js ../api-mocks.js"
  iterate "$1" iter_listCodeCommands
}

declare -r SCRIPTNAME=$0
main "$@"

# Latest utput produced:
# code --reuse-window ./restaurants/_.js ../api-mocks.js
# code --reuse-window ./restaurants/106 - CotéLounge.json ./r/106/*.json
# code --reuse-window ./restaurants/107 - Gilardi.json ./r/107/*.json
# code --reuse-window ./restaurants/108 - IlVicoletto.json ./r/108/*.json
# code --reuse-window ./restaurants/109 - Jach.json ./r/109/*.json
# code --reuse-window ./restaurants/110 - LaCucina.json ./r/110/*.json
# code --reuse-window ./restaurants/111 - LaFemmeDuBoulanger.json ./r/111/*.json
# code --reuse-window ./restaurants/112 - LaMassaia.json ./r/112/*.json
# code --reuse-window ./restaurants/113 - LaPizzaCresci.json ./r/113/*.json
# code --reuse-window ./restaurants/114 - LaTaverneMassena.json ./r/114/*.json
# code --reuse-window ./restaurants/115 - LeLodge.json ./r/115/*.json
# code --reuse-window ./restaurants/116 - LeMagenta.json ./r/116/*.json
# code --reuse-window ./restaurants/117 - LeQuébec.json ./r/117/*.json
# code --reuse-window ./restaurants/118 - Les2Terrasses.json ./r/118/*.json
# code --reuse-window ./restaurants/119 - LeSaintValentin.json ./r/119/*.json
# code --reuse-window ./restaurants/120 - Lobsta.json ./r/120/*.json
# code --reuse-window ./restaurants/121 - Mama.json ./r/121/*.json
# code --reuse-window ./restaurants/122 - Milos.json ./r/122/*.json
# code --reuse-window ./restaurants/123 - MorisBar.json ./r/123/*.json
# code --reuse-window ./restaurants/124 - Pinnochio.json ./r/124/*.json
# code --reuse-window ./restaurants/125 - Rina.json ./r/125/*.json
# code --reuse-window ./restaurants/126 - Sapori.json ./r/126/*.json
# code --reuse-window ./restaurants/127 - VIPsGrill.json ./r/127/*.json
