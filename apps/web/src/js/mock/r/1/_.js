import de from './de.json';
import en from './en.json';
import fr from './fr.json';
import it from './it.json';
import ru from './ru.json';
import zh from './zh.json';

export default {
  ru,
  zh,
  it,
  de,
  en,
  fr,
};
