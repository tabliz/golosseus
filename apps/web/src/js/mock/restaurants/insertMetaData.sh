#/bin/bash

# for file in 1*.json;
# do
#   echo $file;
#   id=$(expr "$file" : "\(1..\) - .*\.json")
#   img="r$id.jpg"
#   sed -i '' "s/\"cover\": \"\"/\"cover\": \"${img}\"/" "$file"
# done

while IFS=\; read -r id name rate votes range desc address phone url
do
  echo
  echo "I got: $id / $name / $rate / $votes / $range / $desc / $address / $phone / $url"
  file=$id*
  [[ "$rate" == "" ]] && rate="0"
  [[ "$votes" == "" ]] && votes="0"
  # [[ "$address" == "" ]] && address="@@ADDRESS@@"
  # [[ "$url" == "" ]] && url="@@URL@@"
  # [[ "$tel" == "" ]] && tel="@@TEL@@"
  echo $file
  sed -i '' "s/\"address\": \"@@ADDRESS@@\",/\"address\": \"$address\",/g" $file
  sed -i '' "s/\"tel\": \"@@TEL@@\",/\"tel\": \"$phone\",/g" $file

  url=$(echo ${url//\//\\/})
  sed -i '' "s/\"url\": \"@@URL@@\",/\"url\": \"$url\",/g" $file
  sed -i '' "s/\"url\": \"https:\/\/\",/\"url\": \"$url\",/g" $file

  sed -i '' "s/\"priceRange\": \".*\",/\"priceRange\": \"$range\",/g" $file

  desc=$(echo ${desc//&/\\&})
  sed -i '' "s/\"fr\": \"@@DESC_FR@@\",/\"fr\": \"$desc\",/g" $file
  rate=$(echo "$rate" | tr "," ".")
  sed -i '' -E "s/\"stars\": [0-9\.]+,/\"stars\": $rate,/g" $file
  sed -i '' -E "s/\"votes\": [0-9]+,/\"votes\": $votes,/g" $file

  sed -i '' -E "s/\"1\": [0-9]+,/\"1\": 0,/g" $file
  sed -i '' -E "s/\"2\": [0-9]+,/\"2\": 0,/g" $file
  sed -i '' -E "s/\"3\": [0-9]+,/\"3\": 0,/g" $file
  sed -i '' -E "s/\"4\": [0-9]+,/\"4\": 0,/g" $file
  sed -i '' -E "s/\"5\": [0-9]+/\"5\": 0/g" $file
done < meta.csv
