import { DebugQS } from '../../utils-common';

import LePhare001 from './001 - LePhare.json';

/*
import LeTrefle002 from './002 - LeTrefle.json';
import EastStret003 from './003 - EastStreet.json';
import RomagnaMia004 from './004 - RomagnaMia.json';
*/

import Portovenere100 from './100 - Portovenere.json';
import BazarCafe101 from './101 - BazarCafe.json';
import Byblos102 from './102 - Byblo\'s.json';
import LeCaféDeNice103 from './103 - LeCaféDeNice.json';
import ClubPlayerCaffé104 from './104 - ClubPlayerCaffé.json';
import LaLorraine105 from './105 - LaLorraine.json';
import CotéLounge106 from './106 - CotéLounge.json';
import Gilardi107 from './107 - Gilardi.json';
import IlVicoletto108 from './108 - IlVicoletto.json';
import Jach109 from './109 - Jach.json';
import LaCucina110 from './110 - LaCucina.json';
import LaFemmeDuBoulanger111 from './111 - LaFemmeDuBoulanger.json';
import LaMassaia112 from './112 - LaMassaia.json';
import LaPizzaCresci113 from './113 - LaPizzaCresci.json';
import LaTaverneMassena114 from './114 - LaTaverneMassena.json';
import LeLodge115 from './115 - LeLodge.json';
import LeMagenta116 from './116 - LeMagenta.json';
import LeQuébec117 from './117 - LeQuébec.json';
import Les2Terrasses118 from './118 - Les2Terrasses.json';
import LeSaintValentin119 from './119 - LeSaintValentin.json';
import Lobsta120 from './120 - Lobsta.json';
import Mama121 from './121 - Mama.json';
import Milos122 from './122 - Milos.json';
import MorisBar123 from './123 - MorisBar.json';
import Pinnochio124 from './124 - Pinnochio.json';
import Rina125 from './125 - Rina.json';
import Sapori126 from './126 - Sapori.json';
import VIPsGrill127 from './127 - VIPsGrill.json';

const D = DebugQS('api-mocks.js > restaurants mocks');

const MOCK_RESTAURANTS_TEMPLATE = {
  restaurants: [
    /*
    LeTrefle002,
    EastStret003,
    RomagnaMia004,
    */
    LePhare001,
    Pinnochio124,
    Sapori126,
    Jach109,
    ClubPlayerCaffé104,
    LeMagenta116,
    Milos122,
    LaTaverneMassena114,
    LeCaféDeNice103,
    Les2Terrasses118,
    LaPizzaCresci113,
    LeQuébec117,
    LaLorraine105,
    CotéLounge106,
    Byblos102,
    MorisBar123,
    BazarCafe101,
    IlVicoletto108,
    Mama121,
    Rina125,
    LaMassaia112,
    LaFemmeDuBoulanger111,
    LaCucina110,
    LeLodge115,
    Portovenere100,
    VIPsGrill127,
    LeSaintValentin119,
    Lobsta120,
    Gilardi107,
  ],
};

function getRestaurants(lng) {
  // Start with a fresh copy of the template (no shallow opy !)
  const HTTP_RESPONSE = JSON.parse(JSON.stringify(MOCK_RESTAURANTS_TEMPLATE));

  // Then replace the map of "summary" with its only key for the requested language
  HTTP_RESPONSE.restaurants.forEach((r) => {
    r.summary = r.summary[lng]; // eslint-disable-line no-param-reassign
  });

  // That's it :)
  D('Built restaurants mock for lng "%s": %o', lng, HTTP_RESPONSE);
  return HTTP_RESPONSE;
}

export default {
  de: getRestaurants('de'),
  en: getRestaurants('en'),
  fr: getRestaurants('fr'),
  it: getRestaurants('it'),
  ru: getRestaurants('ru'),
  zh: getRestaurants('zh'),
};
