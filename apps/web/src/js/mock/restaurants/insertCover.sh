#/bin/bash

for file in 1*.json;
do
  echo $file;
  id=$(expr "$file" : "\(1..\) - .*\.json")
  img="r$id.jpg"
  sed -i '' "s/\"cover\": \"\"/\"cover\": \"${img}\"/" "$file"
done
