import { DebugQS } from './utils-common';

const D = DebugQS('utils-restaurant.js');

const TYPE_ITEM = 'item';
const TYPE_SUBCAT = 'subcategory';
const HM = '(hm)';

const CLASS_MENU_ITEM = 'menu-item';
const CLASS_SUBCAT = 'menu-subcategory';
const CLASS_HANDMADE = 'hand-made';

const SEPARATOR = '.';

const isHM = function _isHM(text) {
  return text.includes(HM);
};

const getTitle = function _getTitle(item) {
  let title = '';
  if (item === undefined) {
    D('Undefined menu item, for title ignoring it');
  } else if (item === null) {
    D('Null menu item for title, ignoring it');
  } else if (item.title === undefined) {
    D('Undefined menu item title, ignoring it');
  } else if (item.title === null) {
    D('Null menu item title, ignoring it');
  } else {
    ({ title } = item);
  }
  return title;
};

const getDesc = function _getDesc(item) {
  let desc = '';
  if (item === undefined) {
    D('Undefined menu item for desc, ignoring it');
  } else if (item === null) {
    D('Null menu item for desc, ignoring it');
  } else if (item.desc === undefined) {
    D('Undefined menu item desc, ignoring it');
  } else if (item.desc === null) {
    D('Null menu item desc, ignoring it');
  } else {
    ({ desc } = item);
  }
  return desc;
};

const getType = function _getType(item) {
  let type = 'menu-item';
  if (item === undefined) {
    D('Undefined menu item for type, ignoring it');
  } else if (item === null) {
    D('Null menu item for type, ignoring it');
  } else if (item.type === undefined) {
    D('Undefined menu item type, ignoring it');
  } else if (item.type === null) {
    D('Null menu item type, ignoring it');
  } else {
    ({ type } = item);
  }
  return type;
};

const isMenuItem = function _isMenuItem(item) {
  return (getType(item) === TYPE_ITEM);
};

const isSubCat = function _iSubCat(item) {
  return (getType(item) === TYPE_SUBCAT);
};

const getCSSClasses = function _getCSSClasses(item) {
  const classes = [];

  // Menu item or menu category ? TODO: use type="subcategory" instead
  if (isMenuItem(item)) {
    classes.push(CLASS_MENU_ITEM);
  } else if (isSubCat(item)) {
    classes.push(CLASS_SUBCAT);
  } else {
    D('getCSSClasses has a pb: neither a menu item nor a menu sub category...');
  }

  // Hand made or not ?
  if (isHM(getTitle(item)) || isHM(getDesc(item))) {
    classes.push(CLASS_HANDMADE);
  }
  return classes.join(', ');
};

const getFullId = function _getFullId(menuPageId, menuItemId) {
  const mi = menuItemId === undefined ? Math.random() : menuItemId;
  return `${menuPageId}${SEPARATOR}${mi}`;
};

const getMenuPageIdFromFullID = function _getMenuPageIdFromFullID(fullID) {
  return fullID.substring(0, fullID.indexOf(SEPARATOR));
};

const getMenuItemIdFromFullID = function _getMenuItemIdFromFullID(fullID) {
  return fullID.substring(fullID.indexOf(SEPARATOR) + 1);
};

export default {
  getCSSClasses,
  getDesc,
  getTitle,
  getType,
  isHM,
  isMenuItem,
  isSubCat,
  getFullId,
  getMenuPageIdFromFullID,
  getMenuItemIdFromFullID,
};
