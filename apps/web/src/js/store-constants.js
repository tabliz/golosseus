export const ACTIONS = {
  // LOAD_RESTAURANTS: 'loadRestaurants',
};

export const GETTERS = {
  ANALYTICS: {
    SESSION_START_TIME: 'sessionStartTime',
    SESSION_FEEDBACK_REQUESTED: 'sessionFeedbackRequested',
  },
  CHOSEN_LNG_TO_CONFIRM: 'chosenLngToConfirm',
  CURRENT_LNG: 'currentLng',
  DATA_OK_TO_LOAD: 'dataOkToLoad',
  DEBUG: 'debug',
  IS_LOADING: 'isLoading',
  MENU_ITEM: 'menuItem',
  RESTAURANT: 'restaurant',
  RESTAURANT_MENU: 'restaurantMenu',
  RESTAURANT_ID: 'restaurantId',
  RESTAURANTS_COUNT: 'restaurantsCount',
};

export const MUTATIONS = {
  ANALYTICS: {
    SET_SESSION_START_TIME: 'setStartTime',
    SET_SESSION_FEEDBACK_REQUESTED: 'setFeedbackRequested',
  },
  SET_CHOSEN_LNG_TO_CONFIRM: 'setChosenLngToConfirm',
  SET_CURRENT_LNG: 'setCurrentLng',
  SET_CHOSEN_MENU_ITEM_FULL_ID: 'setMenuItemFullId',
  SET_CHOSEN_RESTAURANT_ID: 'setRestaurantId',
  SET_DATA_OK_TO_LOAD: 'setDataOkToLoad',
  SET_DEBUG: 'setDebug',
  SET_IS_LOADING: 'setIsLoading',
  SET_RESTAURANT_MENU: 'setRestaurantMenu',
  SET_RESTAURANTS: 'setRestaurants',
};
