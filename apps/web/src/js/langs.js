import de from './lang/de';
import en from './lang/en';
import fr from './lang/fr';
import it from './lang/it';
import ru from './lang/ru';
import zh from './lang/zh';

export default {
  de: { translation: de },
  en: { translation: en },
  fr: { translation: fr },
  it: { translation: it },
  ru: { translation: ru },
  zh: { translation: zh },
};

