import { DebugQS } from './utils-common';

const D = DebugQS('store-menu-cache.js');

const MY_CACHE_SIZE = 10;
const cache = new Array(MY_CACHE_SIZE);
let nextIndex = 0;

/**
 * Add the given menuItem in the cache.
 * If cache size is reached, the oldest menuItem will be discarded and replaced.
 *
 * @param {*} menuItem a menuItem instance
 */
const addMenuItem = (menuItem) => {
  D('Adding into cache[%d] the menuItem "%o".', nextIndex, menuItem);
  cache[nextIndex] = menuItem;
  nextIndex += 1;
  if (nextIndex === cache.length) {
    nextIndex = 0;
  }
  D('nextIndex is now %d.', nextIndex);
};

/**
 * Returns the menuItem whose id is given if found, undefined otherwise
 *
 * @param {*} menuItemId a menuItemId to look for
 */
const getMenuItemById = (menuItemId) => {
  const finder = (e, i, arr) => {
    let result = false;
    if (e.id === menuItemId) {
      D('getMenuItemById found menuItemId "%s" at index "%d" (menuItem is "%o").', e.id, i, arr[i]);
      result = true;
    }
    return result;
  };
  return cache.find(finder);
};

/**
 * Returns true if the given menuItemId is in the cache.
 *
 * @param {*} menuItemId a menuItem instance to test presence for
 */
const hasMenuItemById = (menuItemId) => {
  D('Do we have menuItemId "%s"?', menuItemId);
  const found = (getMenuItemById(menuItemId) !== undefined);
  D('%s', found !== undefined ? 'Yes!' : 'Naaaa.');
  return found;
};

export default {
  addMenuItem,
  getMenuItemById,
  hasMenuItemById,
};
