import Debug from 'debug';
import { GETTERS } from './store-constants';

// const D = Debug('util-commons');

/**
 * Light function to parse query string.
 *
 * @param param The parameter name to parse
 * @param defaultVal The default value to return if the parameter is not found
 * @returns The parsed string, or the defulatVal if it was not present.
 */
const fastQueryStringParsing = function _fastQueryStringParsing(param, defaultVal) {
  const TOKEN = `${param}=`;
  let value = (defaultVal != null ? defaultVal : '');
  if (document && document.location && document.location.search) {
    if (document.location.search.indexOf(TOKEN) >= 0) {
      value = (document.location.search.split(TOKEN)[1].split('&')[0]); // eslint-disable-line prefer-destructuring
    }
  }
  // D('Parsed query string parameter "%s" value: "%s"', param, value);
  return value;
};

/**
 * Parse a boolean in the query string
 *
 * @param param The parameter name to parse
 * @returns The parsed boolean value, or false if not found
 */
const fastQueryStringBooleanParsing = function _fastQueryStringBooleanParsing(param) {
  return Boolean(fastQueryStringParsing(param, 'false').toLowerCase() === 'true');
};

/**
 * Constructor function, that return a logger function that will log
 * according to vm's store debug state getter.
 *
 * @param {*} vm The Vue Model instance
 * @param {*} name Name (prefix) to use for the log
 */
const debugVM = function _debugVM(vm, name) {
  // D('Building a debug function "%s", based on vm.$store.getters[GETTERS.DEBUG]', name);
  const originalDebug = Debug(name);
  const myDebug = function _myDebug(formatter, ...args) {
    if (vm.$store.getters[GETTERS.DEBUG]) {
      if (args !== undefined) {
        originalDebug(formatter, ...args);
      } else {
        originalDebug(formatter);
      }
    }
  };
  return myDebug;
};

/**
 * Constructor function, that return a logger function that will log
 * according to the presence of debug=true parameter in browser's query string
 *
 * @param {*} name Name (prefix) to use for the log
 */
const debugQS = function _debugQS(name) {
  const DEBUG_PARAM = 'debug';
  const debugActive = Boolean(fastQueryStringParsing(DEBUG_PARAM, 'false').toLowerCase() === 'true');
  /*
   D('Building a debug function "%s", based on query string "%s" with value "%s"',
    name,
    DEBUG_PARAM,
    debugActive
  );
   */
  const originalDebug = Debug(name);
  const myDebug = function _myDebug(formatter, ...args) {
    if (debugActive) {
      if (args !== undefined) {
        originalDebug(formatter, ...args);
      } else {
        originalDebug(formatter);
      }
    }
  };
  return myDebug;
};

export const DebugVM = debugVM;
export const DebugQS = debugQS;
export const FastQueryStringParsing = fastQueryStringParsing;
export const FastQueryStringBooleanParsing = fastQueryStringBooleanParsing;
