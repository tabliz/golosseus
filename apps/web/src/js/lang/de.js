export default {
  global: {
    motto: 'Tabliz, schöne Menüs mit Fotos und in Ihrer Sprache, wo immer Sie sind!',
    survey1: 'Wenn Sie ins Ausland reisen, können Sie mit Tabliz Restaurantmenüs immer in deutscher Sprache und mit Fotos anzeigen!',
    survey2: 'Bitte sehen Sie sich das Menü des Restaurants "Le Phare" an und beantworten Sie die Umfrage.',
    tabliz: 'Tabliz',
  },
  home: {
    loading: 'Suche nach Restaurants...',
    welcome: 'Restaurants in der Nähe:',
  },
  restaurant: {
    loading: '{{name}}',
  },
  menuItem: {
    loading: 'Laden...',
  },
  settings: {
    title: 'Einstellungen',
    lng: {
      title: 'Anzeigesprache',
      actions: {
        label: 'Möchten Sie die Anzeigesprache ändern und Tabliz in {{ chosenLng }} verwenden?',
        buttonOk: 'Ja, wechseln Sie zu {{ chosenLng }}',
        buttonCancel: 'Stornieren',
      },
      list: {
        de: 'Deutsch',
        dede: '',
        en: 'Englisch',
        enen: 'English',
        fr: 'Französisch',
        frfr: 'Français',
        it: 'Italienisch',
        itit: 'Italiano',
        ru: 'Russische',
        ruru: 'Русский',
        zh: 'Chinesisch (vereinfachten)',
        zhzh: '中文 (简体)',
      },
    },
    opinionAndHelp: {
      title: 'Rückkopplung',
      answerSurvey: {
        title: 'Meine Meinung geben',
        after: '',
      },
    },
    survey: {
      actions: {
        label: 'Hätten Sie 30 Sekunden, um uns Feedback zu geben?',
        buttonOk: 'Ja natürlich!',
        buttonCancel: 'Später',
      },
    },
  },
  popupError: {
    title: 'Hum, hum !',
    line1: 'Ein Fehler ist aufgetreten... 🙄',
    line2: 'Wir werden das graben, danke für Ihr Verständnis! 😇',
  },
  notFound: {
    title: 'Hum, hum !',
    line1: 'Oups, Es ist ein Fehler...',
    line2: 'Der angeforderte Inhalt {{url}} (ja, der Pfad {{path}}) existiert nicht',
    line3: 'Das ist alles was wir wissen!',
  },
};
