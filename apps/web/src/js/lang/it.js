export default {
  global: {
    motto: 'Tabliz, menu deliziosi con foto e nella tua lingua ovunque tu sia!',
    survey1: 'Quando viaggi all\'estero, puoi utilizzare Tabliz per visualizzare i menu dei ristoranti sempre in italiano e con le foto!',
    survey2: 'Si prega di consultare il menu del ristorante "Le Phare" e rispondere al questionario.',
    tabliz: 'Tabliz',
  },
  home: {
    loading: 'Ricerca ristoranti ...',
    welcome: 'Ristoranti nelle vicinanze:',
  },
  restaurant: {
    loading: '{{name}}',
  },
  menuItem: {
    loading: 'Caricamento in corso ...',
  },
  settings: {
    title: 'Impostazioni',
    lng: {
      title: 'Lingua',
      actions: {
        label: 'Vuoi cambiare la lingua del e usare Tabliz in {{chosenLng}}?',
        buttonOk: 'Sì, passa a {{chosenLng}}',
        buttonCancel: 'Annulla',
      },
      list: {
        de: 'Tedesco',
        dede: 'Deutsch',
        en: 'Inglese',
        enen: 'English',
        fr: 'Francese',
        frfr: 'Français',
        it: 'Italiano',
        itit: '',
        ru: 'Russo',
        ruru: 'Русский',
        zh: 'Cinese (semplificato)',
        zhzh: '中文 (简体)',
      },
    },
    opinionAndHelp: {
      title: 'Feedback',
      answerSurvey: {
        title: 'Dai la mia opinione',
        after: '',
      },
    },
    survey: {
      actions: {
        label: 'Avresti 30 secondi per darci un feedback?',
        buttonOk: 'Sì, certo!',
        buttonCancel: 'Più tardi',
      },
    },
  },
  popupError: {
    title: 'Hum, hum !',
    line1: 'C\'è stato un errore ... 🙄',
    line2: 'Lo scaveremo, grazie per la tua comprensione! 😇',
  },
  notFound: {
    title: 'Hum, hum !',
    line1: 'Oops, è un errore ...',
    line2: 'Il contenuto richiesto {{url}} (sì, il percorso {{path}}) non esiste',
    line3: 'Questo è tutto ciò che sappiamo.',
  },
};
