export default {
  global: {
    motto: 'Tabliz, прекрасные меню с фотографиями и на вашем языке, где бы вы ни были!',
    survey1: 'Когда вы выезжаете за границу, вы можете использовать Tabliz, чтобы проконсультироваться с меню ресторана всегда на русском и с фотографиями!',
    survey2: 'Пожалуйста, просмотрите меню ресторана «Le Phare» и ответьте на опрос.',
    tabliz: 'Tabliz',
  },
  home: {
    loading: 'поиск ресторанов ...',
    welcome: 'Рестораны поблизости:',
  },
  restaurant: {
    loading: '{{name}}',
  },
  menuItem: {
    loading: 'Загрузка ...',
  },
  settings: {
    title: 'Настройки',
    lng: {
      title: 'Язык отображения',
      actions: {
        label: 'Вы хотите изменить язык дисплея и использовать Tabliz в {{selectedLng}}?',
        buttonOk: 'Да, переключитесь на {{selectedLng}}',
        buttonCancel: 'Отмена',
      },
      list: {
        de: 'немецкий',
        dede: 'Deutsch',
        en: 'английский',
        enen: 'English',
        fr: 'Французский',
        frfr: 'Français',
        it: 'Итальянский',
        itit: 'Italiano',
        ru: 'Русский',
        ruru: '',
        zh: 'Китайский (упрощенный)',
        zhzh: '中文 (简体)',
      },
    },
    opinionAndHelp: {
      title: 'обратная связь',
      answerSurvey: {
        title: 'Дайте мое мнение',
        after: '',
      },
    },
    survey: {
      actions: {
        label: 'У вас есть 30 секунд, чтобы дать нам обратную связь?',
        buttonOk: 'Да, конечно!',
        buttonCancel: 'позже',
      },
    },
  },
  popupError: {
    title: 'Hum, hum !',
    line1: 'Произошла ошибка ... 🙄',
    line2: 'Давайте расчитаем это, спасибо за ваше понимание! 😇 ',
  },
  notFound: {
    title: 'Hum, hum !',
    line1: 'Ой, это ошибка ...',
    line2: 'Запрошенный контент {{url}} (да, путь {{путь}} не существует,',
    line3: 'это все, что мы знаем',
  },
};
