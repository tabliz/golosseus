export default {
  global: {
    motto: 'Tabliz, des menus qui donnent envie, en photos et dans votre langue où que vous soyez!',
    survey1: 'Quand vous êtes en voyage à l\'étranger, vous pouvez utiliser Tabliz pour consulter les menus des restaurants toujours en français et avec des photos!',
    survey2: 'Merci de consulter le menu du restaurant "Le Phare" et de répondre ensuite au questionnaire.',
    tabliz: 'Tabliz',
  },
  home: {
    loading: 'Recherche des restaurants...',
    welcome: 'Restaurants à proximité: ',
  },
  restaurant: {
    loading: '{{name}}',
  },
  menuItem: {
    loading: 'Chargement...',
  },
  settings: {
    title: 'Réglages',
    lng: {
      title: 'Langue d\'affichage',
      actions: {
        label: 'Voulez-vous changer la langue d\'affichage et utiliser Tabliz en {{ chosenLng }} ?',
        buttonOk: 'Oui, basculer en {{ chosenLng }}',
        buttonCancel: 'Annuler',
      },
      list: {
        de: 'Allemand',
        dede: 'Deutsch',
        en: 'Anglais',
        enen: 'English',
        fr: 'Français',
        frfr: '',
        it: 'Italien',
        itit: 'Italiano',
        ru: 'Russe',
        ruru: 'Русский',
        zh: 'Chinois (simplifié)',
        zhzh: '中文 (简体)',
      },
    },
    opinionAndHelp: {
      title: 'Feedback',
      answerSurvey: {
        title: 'Donner mon avis',
        after: '',
      },
    },
    survey: {
      actions: {
        label: 'Auriez-vous 30 secondes pour nous faire un feedback?',
        buttonOk: 'Oui bien sûr !',
        buttonCancel: 'Plus tard',
      },
    },
  },
  popupError: {
    title: 'Hum, hum !',
    line1: 'Une erreur s\'est produite... 🙄',
    line2: 'Nous allons creuser ça, merci de votre compréhension ! 😇',
  },
  notFound: {
    title: 'Hum, hum !',
    line1: 'Oups, c\'est une erreur...',
    line2: 'Le contenu demandé {{url}} (oui, le chemin {{path}}) n\'existe pas',
    line3: 'C\'est tout ce que nous savons.',
  },
};
