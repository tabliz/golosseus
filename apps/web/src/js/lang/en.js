export default {
  global: {
    motto: 'When you are travelling Tabliz, lovely menus, with photos and in your language wherever you are!',
    survey1: 'When you are traveling abroad, you can use Tabliz to view restaurant menus always in English and with photos!',
    survey2: 'Please view the menu of "Le Phare" restaurant and answer the survey.',
    tabliz: 'Tabliz',
  },
  home: {
    loading: 'Searching restaurants...',
    welcome: 'Nearby restaurants:',
  },
  restaurant: {
    loading: '{{name}}',
  },
  menuItem: {
    loading: 'Loading...',
  },
  settings: {
    title: 'Settings',
    lng: {
      title: 'Display language',
      actions: {
        label: 'Do you want to change the display language and use Tabliz in {{ chosenLng }}?',
        buttonOk: 'Yes, switch to {{ chosenLng }}',
        buttonCancel: 'Cancel',
      },
      list: {
        de: 'German',
        dede: 'Deutsch',
        en: 'English',
        enen: '',
        fr: 'French',
        frfr: 'Français',
        it: 'Italian',
        itit: 'Italiano',
        ru: 'Russian',
        ruru: 'Русский',
        zh: 'Chinese (simplified)',
        zhzh: '中文 (简体)',
      },
    },
    opinionAndHelp: {
      title: 'Feedback',
      answerSurvey: {
        title: 'Give my opinion',
        after: '',
      },
    },
    survey: {
      actions: {
        label: 'Could you please help us by answering a few quick questions (less than 30s)?',
        buttonOk: 'Sure!',
        buttonCancel: 'Later',
      },
    },
  },
  popupError: {
    title: 'Hum, hum !',
    line1: 'An error occured... 🙄',
    line2: 'We\'re gonna have a deep look into that, thanks for your understanding ! 😇',
  },
  notFound: {
    title: 'Hum, hum !',
    line1: 'Whoops, it\'s an error... 🤨',
    line2: 'The requested content {{url}} (yes, the path {{path}}) does not exist',
    line3: 'That\'s all we know.',
  },
};
