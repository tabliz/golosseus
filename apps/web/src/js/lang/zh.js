export default {
  global: {
    motto: 'Tabliz，可爱的菜单，包括照片和您的语言，无论您身在何处！',
    survey1: '当您出国旅行时，您可以使用Tabliz查询餐厅菜单，中文和照片！',
    survey2: '请查看“Le Phare”餐厅的菜单并回答调查问卷。.',
    tabliz: 'Tabliz',
  },
  home: {
    loading: '寻找餐馆......',
    welcome: '附近的餐馆:',
  },
  restaurant: {
    loading: '{{name}}',
  },
  menuItem: {
    loading: '载入中...',
  },
  settings: {
    title: '设置',
    lng: {
      title: '显示语言',
      actions: {
        label: '是否要更改显示语言并在{{chosenLng}}中使用Tabliz？',
        buttonOk: '是的，切换到{{chosenLng}}',
        buttonCancel: '取消',
      },
      list: {
        de: '德国',
        dede: 'Deutsch',
        en: '英语',
        enen: 'English',
        fr: '法国',
        frfr: 'Français',
        it: '意大利',
        itit: 'Italiano',
        ru: '俄',
        ruru: 'Русский',
        zh: '中文 (简体)',
        zhzh: '',
      },
    },
    opinionAndHelp: {
      title: '反馈',
      answerSurvey: {
        title: '给我的意见',
        after: '',
      },
    },
    survey: {
      actions: {
        label: '你有30秒的时间给我们反馈吗？',
        buttonOk: '是的，当然！',
        buttonCancel: '后来',
      },
    },
  },
  popupError: {
    title: '哼，哼！',
    line1: '发生了错误......🙄',
    line2: '我们将挖掘它，谢谢你的理解！😇',
  },
  notFound: {
    title: '哼，哼！',
    line1: '糟糕，这是一个错误......',
    line2: '请求的内容{{url}}（是的，路径{{path}}）不存在',
    line3: '这就是我们所知道的。',
  },
};
