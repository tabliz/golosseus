# How to name the localizations files

A file here must be named:

- with its ISO 639-1 (alpha-2) code
- optionnally, with its country variant, made of its ISO 3166-1 alpha-2 code. Otheriwse, the above applies.

# Examples

So, we should have:

- fr-FR for French in France
- fr-CA for French in Canada
- fr-BE for French in Belgium
- en-US for American English
- en-GB for British English
- en-ZA for English in south Africa

# Resources

- [List of ISO 639-1 language codes](https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1)
- [List of ISO 3166-1 country codes](https://fr.wikipedia.org/wiki/ISO_3166-1)
