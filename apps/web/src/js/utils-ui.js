import { MUTATIONS } from './store-constants';
import { DebugQS } from './utils-common';

const D = DebugQS('utils-ui.js');

const showLoaderWidget = function _showLoaderWidget(vm, text) {
  vm.$store.commit(MUTATIONS.SET_IS_LOADING, true);
  D('Show loader widget (with passed text "%s")', text);
  vm.$f7.dialog.preloader(text);
};

const hideLoaderWidget = function _hideLoaderWidget(vm) {
  vm.$store.commit(MUTATIONS.SET_IS_LOADING, false);
  D('Hide loader');
  vm.$f7.dialog.close();
};

export default {
  showLoaderWidget,
  hideLoaderWidget,
};
