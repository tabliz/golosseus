class Param {
  constructor(name) {
    this.mName = name;
    this.mRouterName = `:${name}`;
  }
  get name() { return this.mName; }
  get routerName() { return this.mRouterName; }
  toString() {
    return `${this.mName} (${this.mRouterName} for router)`;
  }
}

export const PARAMS = {
  _RID: 'rId',
  _MIID: 'mIId',
  RID: new Param('rId'),
  MIID: new Param('mIId'),
};

export const PATHS = {
  HOME: '/',
  MENU_ITEM: `/r/${PARAMS.RID.routerName}/mi/${PARAMS.MIID.routerName}`,
  RESTAURANT: `/r/${PARAMS.RID.routerName}`,
  SETTINGS: '/settings',
  SETTINGS_LNG: '/settings-lng',
  SETTINGS_ANSWER_SURVEY: '/opinion-and-help/answer-survey',
  SETTINGS_ASK_QUESTION: '/opinion-and-help/ask-question',
  SETTINGS_SUBSCRIBE: '/subscribe',
  STATIC: 'static/', // It is VERY important to not put a starting '/' behind 'static/', otherwise Cordova will not resolve the path
  NOT_FOUND: '(.*)',
};

const getRestaurantUri = function _getRestaurantUri(rId) {
  return PATHS.RESTAURANT.replace(PARAMS.RID.routerName, rId);
};

const getStaticUri = function _getStaticUri(prefix, filename, fallback) {
  let result;
  if (filename !== undefined && filename !== null && typeof filename === 'string' && filename.length > 0) {
    result = PATHS.STATIC + prefix + filename;
  } else {
    result = PATHS.STATIC + prefix + fallback;
  }
  return result;
};

const getMenuItemUri = function _getMenuItemUri(rId, fullID) {
  return PATHS.MENU_ITEM
    .replace(PARAMS.RID.routerName, rId)
    .replace(PARAMS.MIID.routerName, fullID);
};

const getSurveyUri = function _getSurveyUri() {
  return PATHS.SETTINGS_ANSWER_SURVEY;
};

export const UriHelper = Object.freeze({
  getRestaurantUri,
  getStaticUri,
  getMenuItemUri,
  getSurveyUri,
});
