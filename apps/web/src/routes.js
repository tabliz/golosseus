import { PATHS } from './routes-helpers';

import Home from './pages/home.vue';
import MenuItem from './pages/menuItem.vue';
import NotFoundPage from './pages/not-found.vue';
import Restaurant from './pages/restaurant.vue';
import Settings from './pages/settings.vue';
import SettingsLng from './pages/settings-lng.vue';
import AnswerSurvey from './pages/answer-survey.vue';
import AskQuestion from './pages/ask-question.vue';
import Subscribe from './pages/subscribe.vue';
/*
import AboutPage from './pages/about.vue';
import FormPage from './pages/form.vue';
import DynamicRoutePage from './pages/dynamic-route.vue';

import PanelLeftPage from './pages/panel-left.vue';
import PanelRightPage from './pages/panel-right.vue';
*/

export default [
  {
    path: PATHS.HOME,
    component: Home,
  },
  {
    path: PATHS.MENU_ITEM,
    component: MenuItem,
  },
  {
    path: PATHS.RESTAURANT,
    component: Restaurant,
  },
  {
    path: PATHS.SETTINGS,
    component: Settings,
  },
  {
    path: PATHS.SETTINGS_LNG,
    component: SettingsLng,
  },
  {
    path: PATHS.SETTINGS_ANSWER_SURVEY,
    component: AnswerSurvey,
  },
  {
    path: PATHS.SETTINGS_ASK_QUESTION,
    component: AskQuestion,
  },
  {
    path: PATHS.SETTINGS_SUBSCRIBE,
    component: Subscribe,
  },
  {
    path: PATHS.NOT_FOUND, // Must be the last
    component: NotFoundPage,
  },
];
