# Origin
This originates from https://github.com/framework7io/framework7-template-vue-webpack

# Run the app

```
npm start
```

App will be opened in browser at `http://localhost:8080/`

# Build app for production

```
npm run build
```

The output will be at `www/` folder

# Use with cordova

Just put the contents of `www` folder in your cordova's project root `www` folder

# Tweaking

CSS is in src/css/app.css
