import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

import MenuCache from './store-menu-cache';

Vue.use(Vuex);

// Root state object. Each Vuex instance is just a single state tree.
const myState = {
  isLoading: false,
  menu: null,
  menuCache: MenuCache,
  placeId: null,
  places: [],
};

// Mutations are operations that actually mutates the state.
// Each mutation handler gets the entire state tree as the first argument,
// followed by additional payload arguments.
// Mutations must be synchronous and can be recorded by plugins for debugging purposes.
const mutations = {
  setIsLoading(state, isLoading) {
    state.isLoading = isLoading; // eslint-disable-line no-param-reassign
  },
  setMenu(state, menu) {
    state.menu = menu; // eslint-disable-line no-param-reassign
  },
  setPlaces(state, places) {
    state.places = places; // eslint-disable-line no-param-reassign
  },
  setPlaceId(state, id) {
    state.placeId = id; // eslint-disable-line no-param-reassign
    state.menu = []; // eslint-disable-line no-param-reassign
  },
};

// Actions are functions that cause side effects and can involve
// asynchronous operations.
const actions = {
};

// Getters are functions
const getters = {
  place: () => myState.places.find(place => (place.id === myState.placeId)),
};

// A basic logger
const logger = createLogger({
  collapsed: true, // auto-expand logged mutations

  filter(mutation, stateBefore, stateAfter) { // eslint-disable-line no-unused-vars
    return true;
  },
  transformer(state) {
    // transforme l'état avant de le logguer.
    // retourne par exemple seulement un sous-arbre spécifique
    return state;
  },
  mutationTransformer(mutation) {
    // les mutations sont logguées au format `{ type, payload }`
    // nous pouvons les formater comme nous le souhaitons.
    return mutation; // mutation..type
  },
  logger: console, // implementation de l'API `console`, par défaut `console`
});

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  state: myState,
  getters,
  actions,
  mutations,
  plugins: [logger],
});
