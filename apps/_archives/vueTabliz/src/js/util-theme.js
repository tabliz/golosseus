// Import F7 and its Dom7 library
import Debug from 'debug-levels';
import Device from 'current-device';

const d = Debug('util-theme.js');

// The following is a little enum to store the styling info to apply
const myTheme = { MD: 'material', IOS: 'ios', current: undefined };

/**
 * Find out what is the CSS Style to use here.
 * Note we default to Material Design when we can't find.
 *
 * @param qs The style override in query string, if any (could be null, undefined or a string)
 */
const getTheme = (style) => {
  if (myTheme.current === undefined) {
    // Let's go for a lazy init
    let r = '';
    if (style) {
      // Override the style with the one given in parameter.
      // If not ios, we fall back to md.
      r = (style === 'ios' ? myTheme.IOS : myTheme.MD);
      d.debug('Style was overriden in query string: "%s".', r);
    } else {
      // Otherwise detect the underlying OS. Default to md, if not iOS or macOS.
      r = ((Device.ios() || Device.macos() ? myTheme.IOS : myTheme.MD));
      d.debug('Style was detected from device: "%s".', r);
    }
    // Store the style in in our little enum
    myTheme.current = r;
  } else {
    d.debug('Style was already set to "%s".', myTheme.current);
  }
  return myTheme.current;
};

/**
 * Returns true when the theme has been set and is Material Design.
 * Otherwise return false.
 */
const isMdTheme = () => getTheme() === myTheme.MD;

/**
 * Returns true when the theme has been set and is iOS.
 * Otherwise return false.
 */
const isIosTheme = () => getTheme() === myTheme.IOS;

export default {
  Theme: myTheme,
  getTheme,
  isMdTheme,
  isIosTheme,
};
