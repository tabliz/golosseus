export default [
  {
      path: '/about/',
      component: require('./pages/about.vue')
  },
  {
      path: '/form/',
      component: require('./pages/form.vue')
  },
  {
      path: '/dynamic-route/blog/:blogId/post/:postId/',
      component: require('./pages/dynamic-route.vue')
  },
  {
      path: '/place/:placeId/',
      component: require('./pages/place.vue')
  },
  {
    path: '/place/:placeId/menuItem/:menuItemId/',
    component: require('./pages/menuItem.vue')
  }
]
