/*
  TODO:
  - Reset swiper pagination on top, without disabling the clicks on the slides/menu categories
  - Dragging (not dropping) an item from a list is enabled: disable it (Lucy)
  - Redirect unmatched route to home
  - i18n
  -   <f7-nav-left>
        <f7-link icon="icon-bars" open-panel="left"></f7-link>
      </f7-nav-left>
  - Add a Pull to refresh on home page
  - Better error handling
  - Open a custom prelaoder when loading places...
  - Mutations or Actions in the store ? (side effect)
  - Ajouter une animation "bounce" sur les "<" et ">"" lors de leur première apparition
    (pour inviter à leur utilisation)
  - Add a search bar later (Lucy)
  */

/* global window:false, Dom7:false */

import Vue from 'vue';
import Framework7 from 'framework7'; // eslint-disable-line no-unused-vars
import Framework7Vue from 'framework7-vue';
import VueResource from 'vue-resource';
import VueResourceMock from 'vue-resource-mock';

import Icon from 'vue-awesome'; /* /components/Icon'; */
import 'vue-awesome/icons/bars';
import 'vue-awesome/icons/chevron-left';
import 'vue-awesome/icons/chevron-right';
import 'vue-awesome/icons/angle-left';
import 'vue-awesome/icons/angle-right';
import { StarRating } from 'vue-rate-it';
import Debug from 'debug-levels';

import App from './components/app.vue';
import AppStyles from './css/app.css'; // eslint-disable-line no-unused-vars
import Routes from './routes';
import store from './js/store';
import UtilTheme from './js/util-theme';
import MY_LOCAL_API from './js/static-api';

const d = Debug('main.js');

/**
 * Parses the given parameter in query string.
 * @param {*} paramName  the parameter name to look for in uqerys tring.
 * @returns The aprameter value if found, null otherwise.
 */
const getQueryParam = (paramName) => {
  let paramValue = null;
  const query = window.location.search.substring(1); // eslint-disable-line no-undef
  // d.debug('getQueryParam: query string is ', query);
  const params = query.split('&');
  // d.debug('getQueryParam: params are %o', params);
  params.forEach((element) => {
    const pair = element.split('=');
    // d.debug('getQueryParam: parsed pair %o', pair);
    if (decodeURIComponent(pair[0]) === paramName) {
      paramValue = decodeURIComponent(pair[1]);
    }
  });
  d.debug('getQueryParam: %s is ', paramName, paramValue);
  return paramValue;
};

// Import relevant CSS style
const theme = UtilTheme.getTheme(getQueryParam('style'));
d.log('Theme set is %s', theme);

// Configure Vue
Vue.use(Framework7Vue);
Vue.component('icon', Icon);
Vue.component('star-rating', StarRating);
Vue.use(VueResource);
Vue.http.options.root = '/api';

// In this 1st version, we hardcode he api to be locally intercepted by VueResourceMock

/*
if (getQueryParam('mock') === 'yes') {
  Vue.use(VueResourceMock, MY_MOCK_API, { silent: false });
}
*/
Vue.use(VueResourceMock, MY_LOCAL_API, { silent: false });

// Load appropriate CSS
import(`framework7/dist/css/framework7.${theme}.min.css`).then(() => d.debug(`Loaded F7's CSS: ${theme}`));
import(`framework7/dist/css/framework7.${theme}.colors.min.css`).then(() => d.debug(`Loaded F7's colors: ${theme}`));
import(`./css/app.${theme}.css`).then(() => d.debug(`Loaded app.js and its app.${theme}.js override.`));

// Init App
const APP = new Vue({ // eslint-disable-line no-unused-vars
  el: '#app',
  store,
  components: { // Register component
    app: App,
  },
  mounted() { // Mounted hook
    // `this` est une référence à l'instance de vm
    // d.debug(`Mounted this is: ${JSON.stringify(this)}`);
    d.debug('__Document location is %s.', window.location);
    d.debug('__Query string is %o.', Dom7.parseUrlQuery(window.location));
  },
  template: '<app/>',
  framework7: { // Init Framework7 by passing parameters here
    root: '#app',
    material: UtilTheme.isMdTheme(),
    routes: Routes,
    hideNavbarOnPageScroll: true,
  },
});
