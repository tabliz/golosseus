/* eslint global-require: "off" */

export default [
  {
    path: '/place/:placeId',
    component: require('../pages/place.vue'),
  },
  {
    path: '/place/:placeId/menuItem/:menuItemId',
    component: require('../pages/menu-item.vue'),
  },
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
