/*

Server-render the first AJAX call into the page:
https://vuejsdevelopers.com/2017/08/28/vue-js-ajax-recipes/

i18n:
https://github.com/dkfbasel/vuex-i18n
https://github.com/airbnb/polyglot.js

Import only relevant components (all are laoded now)
http://framework7.io/docs/installation.html#es-module
http://framework7.io/vue/installation.html

Import only needed CSS and icons.

Check out information about Vue Component Extensions router
and routes in the Navigation Router section:
http://framework7.io/vue/init-app.html
*/

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';

import Debug from 'debug-levels';

// import Framework7 from 'framework7';
import Framework7 from 'framework7/dist/framework7.esm.bundle';

// import Framework7Vue from 'framework7-vue';
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle';

// import Dialog from 'framework7/dist/components/dialog/dialog';
// import Popup from 'framework7/dist/components/popup/popup';

// Import Styles
import Framework7Styles from './css/framework7.min.css'; // eslint-disable-line no-unused-vars
import IconsStyles from './css/icons.css'; // eslint-disable-line no-unused-vars
import TablizStyles from './css/tabliz.css'; // eslint-disable-line no-unused-vars

// Import App code
import App from './App';
// import router from './js/router';
import routes from './js/routes';
import store from './js/store';

// Vue.config.productionTip = false;

const d = Debug('main.js');

// Init Vue plugins and F7 components
// Framework7.use([Dialog, Popup]);
Vue.use(Framework7Vue, Framework7);

let myTheme = 'auto';
if (document.location.search.indexOf('theme=') >= 0) {
  myTheme = document.location.search.split('theme=')[1].split('&')[0];
}

d.debug('myTheme is %s', myTheme);

const APP = new Vue({ // eslint-disable-line no-unused-vars
  el: '#app',
  store,
  components: {
    app: App,
  },
  mounted() {
    d.debug('mounted() was here.');
  },
  template: '<app/>',
  framework7: {
    // id: 'com.tabliz.mainapp', // App bundle ID
    name: 'Tabliz',
    // root: '#app',
    routes,
    theme: myTheme,
  },
  // router,
});
