#!/bin/bash

magick \
  -background white \
  -fill "rgb(14,122,254)" \
  -font "Sathu" \
  -size 2000x2000 \
  -gravity center \
  label:Tabliz splash.png

magick \
  -background white \
  -fill "rgb(14,122,254)" \
  -font "Sathu" \
  -size 2000x2000 \
  -gravity center \
  label:Tabliz icon.png

