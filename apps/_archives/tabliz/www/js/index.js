"use strict";

// UI stuffs ----------------------------------------------------------------

const CSS_STYLE = {
  MATERIAL: 'material',
  IOS: 'ios',
  CURRENT: undefined
};

/**
 * Find out what is the CSS Style to use here
 */
const getCssStyle = function () {
  if (CSS_STYLE.CURRENT === undefined) {
    let result = "";
    // Is the style overriden in URL?
    if (document && document.location && document.location.search) {
      if (document.location.search.split(/\?|\&/).includes('style=material')) {
        result = CSS_STYLE.MATERIAL;
      } else if (document.location.search.split(/\?|\&/).includes('style=ios')) {
        result = CSS_STYLE.IOS;
      }
    } else if (Framework7.prototype.device.android === true || (navigator && navigator.platform && navigator.platform.match(/(Win)/i))) {
      result = CSS_STYLE.MATERIAL;
    } else if (Framework7.prototype.device.ios === true || (navigator && navigator.platform && navigator.platform.match(/(Mac)/i))) {
      result = CSS_STYLE.IOS;
    }
    if (result === undefined) {
      // We default to material when we can't find what tyle to sue (but this is weird!)
      result = CSS_STYLE.MATERIAL;
    }
    CSS_STYLE.CURRENT = result;
  }
  return CSS_STYLE.CURRENT;
}
const hasMaterialStyle = function () { return getCssStyle() === CSS_STYLE.MATERIAL; }
const hasIosStyle = function () { return getCssStyle() === CSS_STYLE.IOS; }

// Main entry point ---------------------------------------------------------

const whenDeviceReady = function () {
  const $$ = Dom7;

  // Here we make the difference between the iOS / Android styles
  if (hasMaterialStyle()) {
    // Change class
    $$('.view.navbar-through').removeClass('navbar-through').addClass('navbar-fixed');

    // And move Navbar into Page
    $$('.view .navbar').prependTo('.view .page');
  }

  Template7.global = {
    android: hasMaterialStyle(),
    ios: hasIosStyle(),
  };

  // Next, build the app.
  const myApp = APP.myApp = new Framework7({
    material: hasMaterialStyle() ? true : false, // Enable Material theme for Android device only
    template7Pages: true,                        // Enable Template7 pages rendering for Ajax and Dynamic pages
    template7Data: {
      'url:trestaurant.html': [
        {
          name: 'Restaurant 11',
          address: 'contact@restaurant11.com'
        },
        {
          name: 'Restaurant 22',
          address: 'contact@restaurant22.com'
        }
      ]
    },                           // Init template data
    debug: false,                                // Enable debug plugin
    animateNavBackIcon: true                     // For iOS theme only (ignored otherwise)
  });

  // Add view
  const mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar for iOS
    // (F7 will ignore it for material style)
    dynamicNavbar: true
  });

  // Callbacks to run specific code for specific pages, for example for About page:
  myApp.onPageInit('about', function (page) {
    // run createContentPage func after link was clicked
    $$('.create-page').on('click', function () {
      createContentPage();
    });
  });

  // Templates data
  Template7.data['url:trestaurant.html'] = [
    {
      name: 'Restaurant 1',
      address: 'contact@restaurant1.com'
    },
    {
      name: 'Restaurant 2',
      address: 'contact@restaurant2.com'
    }
  ];

  // Load templates
  APP.myTemplates = {};
  const templateLoadSuccess = function (data, status, xhr) {
    APP.myTemplates[xhr.requestUrl] = Template7.compile(data);
    console.info('Template7 "' + xhr.requestUrl + '" loaded.');
  };
  const templateLoadError = function (xhr, status) {
    APP.myTemplates[xhr.requestUrl] = Template7.compile('TEMPLATE DOES NOT EXISTS');
    console.error('Template7 "' + xhr.requestUrl + '" could not be loaded.');
  };
  $$.each([
    'tabout.html',
    'trestaurant.html'], function (index, value) {
      $$.get(value, null, templateLoadSuccess, templateLoadError);
      $$('a[data-template="' + value + '"]').on('click', function () {
        mainView.router.load({
          template: APP.myTemplates[$$(this).attr('data-template')]
        });
      });
    });

  // Generate dynamic page
  var dynamicPageIndex = 0;
  const createContentPage = function () {
    mainView.router.loadContent(
      '<!-- Top Navbar-->' +
      '<div class="navbar">' +
      '  <div class="navbar-inner">' +
      '    <div class="left"><a href="#" class="back link"><i class="icon icon-back"></i><span>Back</span></a></div>' +
      '    <div class="center sliding">Dynamic Page ' + (++dynamicPageIndex) + '</div>' +
      '  </div>' +
      '</div>' +
      '<div class="pages">' +
      '  <!-- Page, data-page contains page name-->' +
      '  <div data-page="dynamic-pages" class="page">' +
      '    <!-- Scrollable page content-->' +
      '    <div class="page-content">' +
      '      <div class="content-block">' +
      '        <div class="content-block-inner">' +
      '          <p>Here is a dynamic page created on ' + new Date() + ' !</p>' +
      '          <p>Go <a href="#" class="back">back</a> or go to <a href="services.html">Services</a> (naaah, no services page yet).</p>' +
      '        </div>' +
      '      </div>' +
      '    </div>' +
      '  </div>' +
      '</div>'
    );
    return;
  }
};

Framework7.prototype.plugins.debug = function (app, params) {
  // exit if not enabled
  if (!params) return;

  return {
    hooks: {
      appInit: function () {
        console.log('appInit');
      },
      navbarInit: function (navbar, pageData) {
        console.log('navbarInit', navbar, pageData);
      },
      pageInit: function (pageData) {
        console.log('pageInit', pageData);
      },
      pageBeforeInit: function (pageData) {
        console.log('pageBeforeInit', pageData);
      },
      pageBeforeAnimation: function (pageData) {
        console.log('pageBeforeAnimation', pageData);
      },
      pageAfterAnimation: function (pageData) {
        console.log('pageAfterAnimation', pageData);
      },
      pageBeforeRemove: function (pageData) {
        console.log('pageBeforeRemove', pageData);
      },
      addView: function (view) {
        console.log('addView', view);
      },
      loadPage: function (view, url, content) {
        console.log('loadPage', view, url, content);
      },
      goBack: function (view, url, preloadOnly) {
        console.log('goBack', view, url, preloadOnly);
      },
      swipePanelSetTransform: function (views, panel, percentage) {
        console.log('swipePanelSetTransform', views, panel, percentage);
      }
    }
  };
};

// Wait for device ready...
const APP = {};
document.addEventListener('deviceready', whenDeviceReady, false);
