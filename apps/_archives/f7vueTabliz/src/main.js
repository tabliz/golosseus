/*

Server-render the first AJAX call into the page:
https://vuejsdevelopers.com/2017/08/28/vue-js-ajax-recipes/

i18n:
https://github.com/dkfbasel/vuex-i18n

Import only relevant components (all are laoded now)
http://framework7.io/docs/installation.html#es-module
http://framework7.io/vue/installation.html

Import only needed CSS and icons.

Check out information about Vue Component Extensions router
and routes in the Navigation Router section:
http://framework7.io/vue/init-app.html

Notes:
https://blog.framework7.io/migration-to-framework7-v2-eb6dc38ede3b
https://stackoverflow.com/questions/44611784/how-to-navigate-to-a-route-in-framework7-vue-app

*/


// Import Axios, Vue, F7, F7 Vue plugin
// import Axios from 'axios';
import Vue from 'vue';
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle';
import Framework7 from 'framework7/dist/framework7.esm.bundle';

// OR: import Framework7 from 'framework7/dist/framework7.esm';
//     import Dialog from 'framework7/dist/components/dialog/dialog';
//     import Popup from 'framework7/dist/components/popup/popup';
// Plus, below:
//     Framework7.use([Dialog, Popup]);

import Debug from 'debug-levels';

// Import Styles
import Framework7Styles from 'framework7/dist/css/framework7.min.css'; // eslint-disable-line no-unused-vars
import myIconsStyle from './css/icons.css'; // eslint-disable-line no-unused-vars
import myTablizStyles from './css/tabliz.css'; // eslint-disable-line no-unused-vars

// Import App code
import myStore from './js/store';
import myRoutes from './routes';

// Import App Component
import myApp from './App.vue';

// Init Vue plugins and F7 components
Vue.use(Framework7Vue, Framework7);

// Make available axios in $http
// Vue.prototype.$http = Axios;

// Override the theme to use if given in browser's url query string
let myTheme = 'auto'; // 'auto', 'ios' or 'md'
if (document.location.search.indexOf('theme=') >= 0) {
  myTheme = document.location.search.split('theme=')[1].split('&')[0]; // eslint-disable-line prefer-destructuring
}

const d = Debug('*** main.js');
d('myTheme is %s', myTheme);

// Init App
const ROOT_VUE_INSTANCE = new Vue({ // eslint-disable-line no-unused-vars
  el: '#app', // Will be used as Framework7 root element
  store: myStore,
  // Register App Component
  components: {
    app: myApp,
  },
  mounted() {
    d('mounted() was here.');
  },
  template: '<app/>',
  // Init Framework7 below
  framework7: {
    id: 'com.tabliz.main-app', // App bundle ID
    name: 'Tabliz',
    navbar: {
      hideOnPageScroll: true,
      scrollTopOnTitleClick: true,
    },
    routes: myRoutes,
    theme: myTheme,
    view: {
      pushState: true,
    },
  },
});
