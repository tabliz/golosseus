export const ACTIONS = Object.freeze({
  LOAD_RESTAURANTS: 'loadRestaurants',
});

export const GETTERS = Object.freeze({
  PLACE: 'loadRestaurants',
});

export const MUTATIONS = Object.freeze({
  SET_IS_LOADING: 'setIsLoading',
  SET_MENU: 'setMenu',
  SET_RESTAURANTS: 'setPlaces',
  SET_RESTAURANTI: 'setPlaceId',
});
