import { MUTATIONS } from './store-enums';

const showLoader = function _showLoader(vm, text) {
  vm.$store.commit(MUTATIONS.SET_IS_LOADING, true);
  vm.$f7.dialog.preloader(text);
};

const hideLoader = function _hideLoader(vm) {
  vm.$store.commit(MUTATIONS.SET_IS_LOADING, false);
  vm.$f7.dialog.close();
};

export default {
  showLoader,
  hideLoader,
};
