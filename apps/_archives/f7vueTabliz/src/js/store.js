/* eslint max-len: ["error", { "comments": 110 }] */
/* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["state"] }] */

import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import Debug from 'debug-levels';

import { ACTIONS, MUTATIONS, GETTERS } from './store-enums';
import MenuCache from './store-menu-cache';

const d = Debug('store.js');

/*
  Mutations are operations that actually mutates the state. Each mutation handler gets the entire state tree
  as the first argument, followed by additional payload arguments.
  Mutations must be synchronous and can be recorded by plugins for debugging purposes.
  They are usable via store.commit('name')
  MUST BE synchronous.

  */
const mutations = {
  [MUTATIONS.SET_IS_LOADING](state, isLoading) { state.isLoading = isLoading; },
  [MUTATIONS.SET_MENU](state, menu) { state.menu = menu; },
  [MUTATIONS.SET_RESTAURANTS](state, places) { state.places = places; },
  [MUTATIONS.SET_RESTAURANTID](state, id) {
    state.placeId = id;
    state.menu = [];
  },
};

// Actions are functions that cause side effects and can involve asynchronous operations.
// They commit the state (not mutate).
// They received context as 1st argument: they do context.commit('name'), context.state or context.getters
const actions = {
  [ACTIONS.LOAD_RESTAURANTS](context) {
    return new Promise((resolve) => {
      this.$http.get('...')
        .then((response) => {
          context.commit(MUTATIONS.SET_RESTAURANTS, response.data);
          resolve();
        })
        .catch((error) => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            d.log(error.response.data);
            d.log(error.response.status);
            d.log(error.response.headers);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            d.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            d.log('Error', error.message);
          }
          d.log(error.config);
        });
    });
  },
};

// Getters are functions, as computed propertyies, but cached based on their deps.
// They get the state as 1st argument, and are exposed as store.getters.<name>
const getters = {
  [GETTERS.PLACE]: (state) => { // eslint-disable-line arrow-body-style
    return state.places.find(place => (place.id === state.placeId));
  },
};

// A basic logger
const logger = createLogger({
  collapsed: true, // auto-expand logged mutations

  // eslint-disable-next-line no-unused-vars
  filter(mutation, stateBefore, stateAfter) {
    return true;
  },
  transformer(state) {
    // transforme l'état avant de le logguer.
    // retourne par exemple seulement un sous-arbre spécifique
    return state;
  },
  mutationTransformer(mutation) {
    // les mutations sont logguées au format `{ type, payload }`
    // nous pouvons les formater comme nous le souhaitons.
    return mutation; // mutation..type
  },
  logger: console, // implementation de l'API `console`, par défaut `console`
});

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
Vue.use(Vuex);
export default new Vuex.Store({
  // Root state object. Each Vuex instance is just a single state tree.
  state: {
    isLoading: false,
    menu: null,
    menuCache: MenuCache,
    placeId: null,
    places: [],
  },
  getters,
  actions,
  mutations,
  plugins: [logger],
});
