import Debug from 'debug-levels';

const d = Debug('store-menu-cache.js');

const MY_CACHE_SIZE = 20;
const cache = new Array(MY_CACHE_SIZE);
let nextIndex = 0;

/**
 * Add the given menuItme in the cache.
 * If cache size is reached, the oldest menuItme will be discarded and replaced.
 *
 * @param {*} menuItem a menuItem instance
 */
const addMenuItem = (menuItem) => {
  d.debug('Adding into cache[%d] the menuItem "%o".', nextIndex, menuItem);
  cache[nextIndex] = menuItem;
  nextIndex += 1;
  if (nextIndex === cache.length) {
    nextIndex = 0;
  }
  d.debug('nextIndex is now %d.', nextIndex);
};

/**
 * Returns the menuItem whose id is given, or undefined otherwise if not found.
 *
 * @param {*} menuItem a menuItemId to look for
 */
const getMenuItemById = (menuItemId) => {
  const finder = (e, i, arr) => {
    let result = false;
    if (e.id === menuItemId) {
      d.debug('getMenuItemById found menuItemId "%s" at index "%d" (menuItem is "%o").', e.id, i, arr[i]);
      result = true;
    }
    return result;
  };
  return cache.find(finder);
};

/**
 * Returns true if the given menuItem is in the cache.
 *
 * @param {*} menuItem a menuItem instance to test presence for
 */
const hasMenuItemById = (menuItemId) => {
  d.debug('Do we have menuItemId "%s"?', menuItemId);
  const found = (getMenuItemById(menuItemId) !== undefined);
  d.debug('%s', found !== undefined ? 'Yes!' : 'Naaaa.');
  return found;
};

export default {
  addMenuItem,
  getMenuItemById,
  hasMenuItemById,
};
