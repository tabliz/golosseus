import PopupAbout from './f7pages/popup-about.vue';
// import PopupCF from './pages/popup-cf.html';
import PopupCFV from './pages/popup-cfv.vue';

const PATH = {
  ROOT: '/',
  POPUP_ABOUTH: '/popup-abouth/',
  POPUP_ABOUTV: '/popup-aboutv/',
  NOT_FOUND: '/not-found/',
};

const COMPONENT_URL = {
  NOT_FOUND: './f7pages/not-found.html',
  POPUP_ABOUTH: './pf7pages/opup-about.html',
  POPUP_ABOUTV: './f7pages/popup-about.vue',
  RESTAURANTS_LIST: './f7pages/restaurants-list.html',
};

export default [
  {
    // name: 'popup-content',
    path: '/popup-content/',
    popup: {
      content: '\
      <div class="popup">\
        <div class="view">\
          <div class="page">\
          Pop-up content here, at {{ $route.params.name }}!\
          </div>\
        </div>\
      </div>',
    },
  },
  {
    path: '/news/',
    content: '\
      <div class="page">\
        <div class="page-content">\
          <div class="block">\
            <p>News. Created dynamically, at {{ $route.params.name }}</p>\
          </div>\
        </div>\
      </div>\
    ',
  },
  // Load Popup from component file
  {
    path: '/popup-cf/',
    loginScreen: {
      componentUrl: './pages/popup-cf.html',
    },
  },
  {
    path: '/popup-cfv/',
    component: PopupCFV,

    /* loginScreen: {
      componentUrl: './pages/popup-cfv.vue',
    }, */
  },
  {
    name: 'root',
    path: PATH.ROOT,
    componentUrl: COMPONENT_URL.RESTAURANTS_LIST,
  },
  {
    name: 'popup-about',
    path: PATH.POPUP_ABOUT,
    componentUrl: COMPONENT_URL.POPUP_ABOUT,
  },
  {
    name: 'popup-abouth',
    path: PATH.POPUP_ABOUTH,
    componentUrl: COMPONENT_URL.POPUP_ABOUTH,
  },
  {
    name: 'popup-aboutv',
    path: PATH.POPUP_ABOUTV,
    componentUrl: COMPONENT_URL.POPUP_ABOUTV,
  },
  {
    name: 'not-found',
    path: PATH.NOT_FOUND,
    componentUrl: COMPONENT_URL.NOT_FOUND,
  },
  {
    name: 'not-found-catchall',
    path: '(.*)',
    componentUrl: COMPONENT_URL.NOT_FOUND,
  },
];
