import HomePage from './_pages/_home.vue';
import AboutPage from './_pages/_about.vue';
import FormPage from './_pages/_form.vue';
import DynamicRoutePage from './_pages/_dynamic-route.vue';
import NotFoundPage from './_pages/_not-found.vue';

import PanelLeftPage from './_pages/_panel-left.vue';
import PanelRightPage from './_pages/_panel-right.vue';

export default [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/home-default',
    component: HomePage,
  },
  {
    path: '/panel-left/',
    component: PanelLeftPage,
  },
  {
    path: '/panel-right/',
    component: PanelRightPage,
  },
  {
    path: '/about/',
    component: AboutPage,
  },
  {
    path: '/form/',
    component: FormPage,
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];
