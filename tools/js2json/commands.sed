s/export default//g

s/"/\\"/g
s/''/""/g
s/\([^\\]\)'/\1"/g

s/\\'/'/g

s/pages: /"pages": /g
s/title_tr: /"title_tr": /g
s/title: /"title": /g
s/id: /"id": /g
s/items: /"items": /g
s/type: /"type": /g
s/desc: /"desc": /g
s/price: /"price": /g
s/cover: /"cover": /g
s/score: /"score": /g
s/stars: /"stars": /g
s/3\.4,/"3.4",/g
s/votes: /"votes": /g
s/dist: /"dist": /g

s/restaurants/"restaurants"/g
s/address/"address"/g
s/name/"name"/g
s/priceRange/"priceRange"/g
s/summary/"summary"/g

s/1: /"1": /g
s/2: /"2": /g
s/3: /"3": /g
s/4: /"4": /g
s/5: /"5": /g

s/};/}/g
